<?php

namespace App\Http\Controllers\V1;

use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateConfigRequest;
use Illuminate\Auth\Access\AuthorizationException;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('getAllConfigs', Config::class);

            $configs = Config::all();

            return response(
                $configs
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $this->authorize('showConfig', Config::class);

            $config = Config::find($id);

            if(!$config) {
                return response(
                    [
                        'errors' => 'The requested Config is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            return response(
                $config
            );
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateConfigRequest $request, $id)
    {
        try {
            $this->authorize('updateConfig', Config::class);

            $config = Config::find($id);

            if(!$config) {
                return response(
                    [
                        'errors' => 'The requested Config is not found'
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $value = $request->input('value');
            $type = $request->input('type');

            $config->type = $type;

            if((Config::TYPE_FLOAT == $type) || (Config::TYPE_INT == $type)) {
                if(!is_numeric($value)) {
                    return response(
                        [
                            'errors' => 'Invalid value entered'
                        ],
                        Response::HTTP_UNPROCESSABLE_ENTITY
                    );
                }

                // convert value formats
                if(Config::TYPE_FLOAT == $type) {
                    $value = (float) $value;
                }

                // convert value formats
                if(Config::TYPE_INT == $type) {
                    $value = (int) $value;
                }
            }

            $config->value = $value;

            $config->update();

            return response($config);
        } catch (AuthorizationException  $ex) {
            return response(
                [
                    'errors' => $ex->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
    }
}
