<?php

namespace App\Http\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConfigPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can get all configs.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function getAllConfigs(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can show config.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function showConfig(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }

    /**
     * Determine whether the user can update config.
     *
     * @param User $user
     * 
     * @return mixed
     */
    public function updateConfig(User $user)
    {
        return Role::ROLE_TYPE_ADMIN == $user->role_id;
    }
}