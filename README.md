# Project Features

      ----------------- USER ---------------------
         * REST APIs
         * User Logging
         * Get Users List
         * User Creation, Remove, Update and Delete
         * Get User Base Details
         * Example of Unit Test - User Creation
         * Example of Unit Test - User Update
         * Example of Unit Test - User Delete
         * User Seeder
      ----------------- USER ---------------------

      ----------------- PRODUCTS ---------------------
         * REST APIs
         * Get Product List
         * Product Creation, Remove, Update and Delete
         * Get Product Base Details
         * Example of Unit Test - Product Creation
         * Example of Unit Test - Product Update
         * Example of Unit Test - Product Delete
         * Product Seeder
         * Product Image Upload
      ----------------- PRODUCTS ---------------------

      ----------------- PRODUCTS ATTRIBUTES ---------------------
         * REST APIs
         * Get Product Attributes List
         * Product Attribute Creation, Remove, Update and Delete
         * Get Product Attribute Base Details
         * Example of Unit Test - Product Attribute Creation
         * Example of Unit Test - Product Attribute Update
         * Example of Unit Test - Product Attribute Delete
         * Product Attribute Seeder
      ----------------- PRODUCTS ATTRIBUTES ---------------------

      ----------------- CONFIGS ---------------------
         * REST APIs
         * Get Configs List
         * Configs Update
         * Example of Unit Test - Config Update
         * Config Seeder
      ----------------- CONFIGS ---------------------

# Project Setup (Please note that the below steps have to be followed in order to be run the project successfully)

   1. Get the clone from the main git branch
   2. Go to the project path
   3. .env.example file rename to .env
   4. Open the CMD (Command Prompt)
   5. Run this command on CMD -- `composer update`
   6. Create a DB as `webmagnat_coding_test`
   7. Run this command on CMD -- `php artisan key:generate`
   8. Run this artisan command on CMD -- `php artisan migrate`
   9. Run this artisan command on CMD -- `php artisan db:seed`

# Project End-points Usage

   1. Get the postman collection project path -- /postmancollection/v1
   2. Run first login enpoint -- username - {admin} / password - {123456}
   3. API will retrun the JWT token
   4. Use this token in every time when calling a endpoint in the header of the authorization token field

# Project Unit Test

   1. Open the CMD (Command Prompt)
   2. Run this php artisan code on CMD - `php artisan test`

# Ex (Logging and Logout) :-

   Loging - 

      API Call ---->

         curl --location 'http://127.0.0.1:8000/api/v1/login' \
         --header 'Accept: application/json' \
         --header 'Content-type: application/json' \
         --form 'username="admin"' \
         --form 'password="123456"'

      API Response ---->

         {
            "jwt": "1|SOWe05x7EmwY19csMHq5FCbwZ0D1r8MGxP5cs8iu"
         }

   Logout -

      API Call ---->

         curl --location --request POST 'http://127.0.0.1:8000/api/v1/logout' \
         --header 'Accept: application/json' \
         --header 'Content-type: application/json' \
         --header 'Authorization: Bearer 1|SOWe05x7EmwY19csMHq5FCbwZ0D1r8MGxP5cs8iu'

      API Response ---->

         {
            "message": "You have been successfully logged out."
         }