<?php

return [
    // ------------------ System Config

    // image file formats which allow from the system
    "IMAGE_EXTENSIONS" => [
        "png",
        "jpg",
        "jpeg",
    ],

    // NOTE :- sizes in KB formats
    // image sizes which allow from the system
    "IMAGE_SIZE" => [
        'max' => '1024',
        'min' => '0',
    ],

    // product photo starage path
    "PRODUCT_PHOTO_BASE_FILE_UPLOAD_STORAGE_PATH" => env('PRODUCT_PHOTO_BASE_FILE_UPLOAD_STORAGE_PATH', 'image_storage/product_photos/'),

    "REGEX" => [
        // 'amount' => '^(?!0*(\.0+)?$)(\d*\.\d{2})$', // with 2 decimal compulsory
        'amount' => '^(?!0(\.0*)?$)\d+(\.?\d{0,2})?$', // 0 values not allowed
    ],

    //------------------- System Config
];

?>