<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Role;
use App\Models\Config;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConfigTest extends TestCase
{
    /**
     * A basic feature test example to update the config
     *
     * @return void
     */
    public function test_config_update()
    {
        // clear the all data
        $this->testInitiateAndClear();

        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $configData = [
            "key" => "p_vat",
            "type"=> Config::TYPE_FLOAT,
            "value" => '0.2',
        ];

        $outData = $this->put(
            '/api/v1/configs/1',
            $configData,
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }
}
