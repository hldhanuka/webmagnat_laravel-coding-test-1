<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Role;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
// use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example to create the product
     *
     * @return void
     */
    public function test_product_create()
    {
        // clear the all data
        $this->testInitiateAndClear();

        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        // ------------------------ set the file
        $fileName = "20230407110900.jpg";
        $file = NULL;

        // get storage directory
        $storageDisk = Storage::disk('product_photo');

        $file = new UploadedFile(
            $storageDisk->path($fileName),
            $fileName,
            'image/jpg',
            null,
            true,
            true
        ); 
        // ------------------------ set the file

        $productData = [
            "product_name" => "Apple iPhone 11",
            "price" => (float) 10,
            // "photo" => $file,
            "attributes" => "{\"colour\":\"1\",\"size\":\"2\"}"
        ];

        $outData = $this->call(
            'POST',
            '/api/v1/products',
            $productData,
            [],
            [
                'photo' => $file
            ],
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt,
                'Accept' => 'application/json'
            ]
        );

        $outData->assertStatus(200);
    }

    /**
     * A basic feature test example to update the product
     *
     * @return void
     */
    public function test_product_update()
    {
        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        // ------------------------ set the file
        $fileName = "20230407110900.jpg";
        $file = NULL;

        // get storage directory
        $storageDisk = Storage::disk('product_photo');

        $file = new UploadedFile(
            $storageDisk->path($fileName),
            $fileName,
            'image/jpg',
            null,
            true,
            true
        ); 
        // ------------------------ set the file

        $productData = [
            "product_name" => "Apple iPhone 10",
            "price" => (float) 10,
            // "photo" => $file,
            "attributes" => "{\"colour\":\"1\",\"size\":\"2\"}"
        ];

        $outData = $this->call(
            'POST',
            '/api/v1/products/2',
            $productData,
            [],
            [
                'photo' => $file
            ],
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt,
                'Accept' => 'application/json'
            ]
        );

        $outData->assertStatus(200);
    }

    /**
     * A basic feature test example to delete the product
     *
     * @return void
     */
    public function test_product_delete()
    {
        // ------------- admin logging
        $authResponse = $this->testAuthLoginWithRole(Role::ROLE_TYPE_ADMIN);

        $authContentWithJason = json_decode($authResponse->getContent());
        // ------------- admin logging

        $outData = $this->delete(
            '/api/v1/products/2',
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $authContentWithJason->jwt
            ]
        );

        $outData->assertStatus(200);
    }
}